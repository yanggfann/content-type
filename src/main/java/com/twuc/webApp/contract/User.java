package com.twuc.webApp.contract;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class User {
    @NotNull
    private String name;
    @Max(100)
    @Min(18)
    private Integer age;
    @Email
    private String email;
    @JsonIgnore
    private String gender;

    public User(String name, Integer age, String email, String gender) {
        this.name = name;
        this.age = age;
        this.email = email;
        this.gender = gender;
    }

    public User() {

    }

    public String getName() {
        return name;
    }

    public Integer getAge() {
        return age;
    }

    @JsonProperty("user_email")
    public String getEmail() {
        return email;
    }

    @JsonProperty("email")
    public void setEmail(String email) {
        this.email = email;
    }

    public String getGender() {
        return gender;
    }
}
