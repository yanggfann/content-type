package com.twuc.webApp;

import com.twuc.webApp.contract.User;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.ValidationException;
import java.util.ArrayList;
import java.util.List;


//String
//Integer
//Object
//List:getMapping,postMapping,deleteMapping
//validation: name不为空，age18~100,email格式正确

@RestController
@RequestMapping("/api")
public class ContentTypeController {

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }

    private List<User> userList = new ArrayList<>();

    //String
    @PostMapping(value = "/name", consumes = "application/json")
    public String createUsermname(@RequestBody String username) {
        return username;
    }

    //Integer
    @PostMapping(value = "/age", consumes = "application/json")
    public Integer createUserAge(@RequestBody Integer userage) {
        return userage;
    }

    //Object(User)
    @PostMapping(value = "/user", consumes = "application/json")
    public User createUser(@RequestBody User user) {
        return user;
    }

    //List  PostMapping
    @PostMapping(value = "/users", consumes = "application/json")
    public List<User> createUsers(@RequestBody List<User> users) {
        for(User user: users){
            userList.add(user);
        }
        return userList;
    }

    //List GetMapping
    @GetMapping(value = "/users")
    public List<User> getUsers() {
        return userList;
    }

    //List DeleteMapping
    @DeleteMapping(value = "/users")
    public int deleteUsers() {
        userList.clear();
        int size = userList.size();
        return size;

    }

    //validation
    @PostMapping(value = "/uservalidation", consumes = "application/json")
    public User validateuser(@RequestBody @Valid User user, Errors errors) {

        if(errors.hasErrors()) {
            throw new ValidationException("valid failed");
        }
        return user;
    }
}
