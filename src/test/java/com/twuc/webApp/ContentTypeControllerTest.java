package com.twuc.webApp;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.webApp.contract.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@AutoConfigureMockMvc
@SpringBootTest
class ContentTypeControllerTest {

	@Autowired
	private MockMvc mockMvc;
	private User user;
	private List<User> users = new ArrayList<>();
	private User user1;

	@Autowired
	private ContentTypeController contentTypeController;

	@BeforeEach
	void setUp() {
		user = new User("xiaowang", 18, null, "female");
		user1 = new User("xiaohua", 22, null, "male");
		users.add(user);
		users.add(user1);
		users.add(user);
	}

	@Test
	void valide_String() throws Exception {
		mockMvc.perform(post("/api/name").contentType("application/json").content("xiaowang"))
				.andExpect(content().string("xiaowang"));
	}

	@Test
	void valide_integer() throws Exception {
		mockMvc.perform(post("/api/age").contentType(MediaType.APPLICATION_JSON_UTF8_VALUE).content("18"))
				.andExpect(content().string("18"));
	}

	@Test
	void valide_Object_user() throws Exception {
		ObjectMapper objectMapper = new ObjectMapper();
		String userAsString = objectMapper.writeValueAsString(user);
		mockMvc.perform(post("/api/user").contentType(MediaType.APPLICATION_JSON_UTF8_VALUE).content(userAsString))
				.andExpect(jsonPath("$.name").value("xiaowang"))
				.andExpect(jsonPath("$.gender").value("female"));
	}

	@Test
	void valide_post_list_users() throws Exception {
		ObjectMapper objectMapper = new ObjectMapper();
		String userAsString = objectMapper.writeValueAsString(users);
		mockMvc.perform(post("/api/users").contentType(MediaType.APPLICATION_JSON_UTF8_VALUE).content(userAsString))
				.andExpect(jsonPath("$[1].name").value("xiaohua"));
	}

	@Test
	void valide_get_list_users() throws Exception {
		contentTypeController.setUserList(users);
		mockMvc.perform(get("/api/users"))
				.andExpect(jsonPath("$[0].name").value("xiaowang"));
	}

	@Test
	void valide_delete() throws Exception {
		mockMvc.perform(delete("/api/users"))
				.andExpect(content().string("0"));
	}
}
